// STARTUP IDs from 0x259 (601) to 0x320 (800)
//maybe start and stop buttons should be in the "RESERVED" column?
#define START_BUTTON_PRESSED 0x259
#define STOP_BUTTON_PRESSED 0x260
#define CAL_REQUEST 0x261
#define SLIP_PERCENTAGE 0x262
#define PRECHARGE_DONE 0x26D

//SENSOR IDs from 0x321 (801) to 0x44C (1100)
#define THROTTLE_POSITION_SENSOR 0x321
#define TEMPERATURE_SENSOR 0x322
#define BRAKE_PRESSURE_FRONT 0x328
#define BRAKE_PRESSURE_BACK 0x329
#define FRONT_WHEELS 0x32B
#define BACK_WHEELS 0x327
#define CURRENT_SENSOR 0x349 //(841)

//BMS IDs (predefined, see the link: http://lithiumate.elithion.com/php/controller_can_specs.php )
#define VOLTAGE_LEVEL 0x681 //uint8_t //information in package 5 and 6

//PING IDs from 0x44D (1101) to 0x47E (1150)
#define FIVE_PINGS_FAULT 0x44D
#define TEN_PINGS_FAULT 0x44E
#define NO_PING_RETURNED 0x44F 
#define PINGS_OK	0x450

#define ECU_DATA_REQUEST 0x451
#define RPI_DATA_REQUEST 0x452
#define PING_SENT 0x453

