#include "BSP_systick.h"

volatile uint16_t clk1000ms=CLK_RESET ; 
volatile uint16_t clk100ms=CLK_RESET ;  
volatile uint16_t clk5000ms=CLK_RESET ;
volatile uint16_t clk2000ms=CLK_RESET ;


void BSP_systick_init(void)
{
	SysTick_Config(SystemCoreClock / 1000);
}

void SysTick_Handler(void)
{
	if (clk5000ms != CLK_COMPLETE){  //5 seconds
		clk5000ms++;
		if (clk5000ms >= 5001) clk5000ms = CLK_COMPLETE;
	}
	
	if (clk2000ms != CLK_COMPLETE){  //5 seconds
		clk2000ms++;
		if (clk2000ms >= 2001) clk2000ms = CLK_COMPLETE;
	}
	
	if (clk1000ms != CLK_COMPLETE){ // 1 second
		clk1000ms++;
		if (clk1000ms >= 1001) clk1000ms = CLK_COMPLETE;
	}

	if (clk100ms != CLK_COMPLETE){ // 0.1 seconds 
		clk100ms++;
		if (clk100ms >= 101) clk100ms = CLK_COMPLETE;
	}   
	
	
	
}
