/* Includes */
#include "appl.h"
#include "PID_Controller.h"
#include "BSP_Traction_control.h"
#include "BSP_Powerlimit.h"


/* Defines */

/*Function prototypes*/
void RTDS_init(void);

/*Variables*/
uint8_t datacan[8] = {1};

uint8_t motor_ARMED = 0;

uint8_t failure_mode = 0;

uint16_t rpm = 0;
uint16_t temporary_rpm = 0;
uint8_t rpmRequest[8] = {1};
uint16_t pid_traction_control_output = 0;
uint16_t pid_power_limit_output = 0;


void arm_motor()
{
	GPIO_SetBits(GPIOC, GPIO_Pin_2);
	motor_ARMED = 1; // used in CAN interrupt handler for accessing data in different driving states
}

void disarm_motor()
{
	GPIO_ResetBits(GPIOC, GPIO_Pin_2);
	motor_ARMED = 0;
}

void RTDS(void)
{
	if(RTDS_ACTIVATE == 1){
			RTDS_init();
	}
}

void RTDS_init(void)
{
		GPIO_SetBits(GPIOC, GPIO_Pin_9);
		clk2000ms = CLK_RESET;
		while(clk2000ms != CLK_COMPLETE);
		GPIO_ResetBits(GPIOC, GPIO_Pin_9);
		RTDS_ACTIVATE = 0;
}


int main(void)
{
	BSP_systick_init();
	BSP_LED_init();
  
	BSP_USART_init();
	BSP_CAN_init();
  BSP_CAN2_init();
	BSP_Interrupt_init();
	BSP_timer_init();
	
	//LED_init();
	//timer_init();
	//PWM_init();
	
	// set a wait period here to get all data logged and stuff?
	
	while(1)
	{
		
		disarm_motor();
		while(!(pings_ok || stop_button)) //startup loop conditions. will be here if the user pressed STOP BUTTON
		{	
			RTDS_ACTIVATE = 0;	

			
		}
		
		arm_motor();
		RTDS();
		while(startup_done && start_button && !(stop_button)) //after startup loop. will be here if the user pressed START BUTTON 
		{	
				
			
			if(new_throttle_data == 1) 
			{
				temporary_rpm =  (int16_t) ((data_throttle[1]<<8) | data_throttle[0]); //data from throttle position sensor buffered from CAN1 bus (8 bit data fields)
				
				pid_traction_control_output = (uint16_t) traction_control(data_back_wheels[0], data_front_wheels[0]);	//traction control algorithm output
				pid_power_limit_output = (uint16_t) powerlimit(data_current_sens, data_voltage_sens);
				
				rpm = pid_traction_control_output + pid_power_limit_output + temporary_rpm ; //variable that will be used for torque request. 
																											//the variable pid_traction_control_output will only reduce rpm or be equal to zero. so ovverflow will not be a problem

				if(rpm < 100){ //fant ut under testing at under 100 s� beveger ikke motoren seg, for lite p�drag. kutter den bare av 
															//could potentially also get negative rpm request due to PID traction control and PID powerlimit, which we dont want. saturate it.
					rpm = 0;
				}
				 //f�rste datafelt (8 bits) i CAN meldingen til motorkontrolleren     			
				rpmRequest[0] = 0x90; //0x31 er RPM adressen i motorkontrolleren      0x90 er torque adressen i motorkontrolleren
				
				//andre datafelt (8 bits) i CAN meldingen til motorkontrolleren
				rpmRequest[1] = 0xFF & rpm;  //fjerner alle ubrukte bits med en OG operasjon med 0xFF
				
				//tredje datafelt (8 bits) i CAN meldingen til motorkontrolleren
				rpmRequest[2] = rpm >> 8;  // etter alle ubrukte bits er fjernet
				
				//sender CAN melding til motorkontroller
				//ID, antall datafelt, og dataen
				BSP_CAN2_Tx(0x210, 3, rpmRequest); //sender verdiene til motorkontroller via CAN buss 2, som er dedikert motorkontroller CAN buss
				
				new_throttle_data = 0;
			}
			
			
		}
		
		while(failure_mode)
		{
			while(1);
		}
		
	}	
}

