#include <stdlib.h>
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_can.h"
#include "misc.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "ringBuffer.h"
#include "BSP_CAN.h"
#include "CAN_ID_LIST.h"

extern uint8_t calib_request;
extern uint8_t usart_id;

void BSP_USART_init(void);
void USART1_IRQHandler(void);
uint8_t BSP_CAN_TX(uint32_t address, uint8_t length, uint8_t data[8]);
void BSP_USART_Rx(uint8_t address, uint8_t data1, uint8_t data2);
void USB_LP_CAN1_RX0_IRQHandler(void);
void BSP_RPI_Send(uint8_t address, uint8_t data1, uint8_t data2);
void UART_SendByte(char byte);

void BSP_USART_puts(volatile char *s);



