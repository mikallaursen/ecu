#include "BSP_CAN.h"

void timer_init(void){
	TIM_TimeBaseInitTypeDef TIM_InitStruct;
	NVIC_InitTypeDef NVIC_initStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);
	
	TIM_InitStruct.TIM_Prescaler = 4; 													
	TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_InitStruct.TIM_Period =  21000;   //1000 Hz
	TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_InitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM8, &TIM_InitStruct);
	TIM_Cmd(TIM8,ENABLE);
	
	
	NVIC_initStruct.NVIC_IRQChannel = TIM8_CC_IRQn;
	NVIC_initStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_initStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_initStruct.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_initStruct);

	TIM_ITConfig(TIM8, TIM_IT_CC2, ENABLE);
}

void LED_init(void){
	GPIO_InitTypeDef GPIO_initStruct;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_initStruct.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOA, &GPIO_initStruct);
}

void PWM_init(void){
	TIM_OCInitTypeDef  TIM_OCStruct;
	
	TIM_OCStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCStruct.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OCStruct.TIM_Pulse = 21000;//5251 ; // /* 25% duty cycle */
	TIM_OC1Init(TIM8, &TIM_OCStruct);
	TIM_OC1PreloadConfig(TIM8, TIM_OCPreload_Enable);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_TIM8);
/*
	pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1
	
	where DutyCycle is in percent, between 0 and 100%
	
	25% duty cycle:     pulse_length = ((42000 + 1) * 25) / 100 - 1 = 10500
*/

	//TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
}

void TIM8_CC_IRQHandler(void){
	__disable_irq();
	if(TIM_GetITStatus(TIM8, TIM_IT_CC2) != RESET){
		
		GPIO_ToggleBits(GPIOA,GPIO_Pin_7);

		TIM_ClearITPendingBit(TIM8, TIM_IT_CC2);
	}
	__enable_irq();
}

