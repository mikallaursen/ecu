//#include "ringBuffer.h"

//void ringBuffer_init(ringBuffer *buffer, int64_t length){
//	//ringBuffer *buffer = calloc(1,sizeof(ringBuffer));
//	buffer->length = length;
//	buffer->head = -1; //the head is the writer
//	buffer->tail = -1; //the tail is the reader
//	buffer->buffer = malloc(sizeof(uint16_t)*length);
//}

//void ringBuffer_free(ringBuffer *buffer){
//	free(buffer->buffer);  //clears the buffer
//}

//bool isEmpty(ringBuffer *buffer){
//	if(buffer->head == -1 && buffer->tail == -1) return true; //when buffer is empty, head & tail = -1
//	return false;
//}


//bool isFull(ringBuffer *buffer){
//	if((buffer->head +1)%buffer->length == buffer->tail) return true; // if the next position for the head is where tail is pointing, the buffer is full
//	return false;
//}

//void ringBuffer_insert(ringBuffer *buffer, uint16_t data){
//		if( isEmpty(buffer) ) { 
//			buffer->head = buffer->tail = 0;  //if the buffer is empty initialize the buffer 	
//			buffer->buffer[buffer->head] = data; //then write data into the buffer position head
//				
//		} else if( isFull(buffer) ){ //if buffer is full, overwrite data and move reader-pointer=the tail
//			buffer->tail = (buffer->tail+1)%buffer->length;
//			buffer->head = (buffer->head+1)%buffer->length;
//			buffer->buffer[buffer->head] = data;	
//			  
//				
//		} else { 
//			buffer->head = (buffer->head+1)%buffer->length;	 // if tail is 0, then (0+1) % 100 = 1, so the tail is 1
//			buffer->buffer[buffer->head] = data;
//		}	
//}

//uint16_t ringBuffer_read(ringBuffer *buffer){  //FIFO--> first in first out. so we read from tail
//	uint16_t tempdata = buffer->buffer[buffer->tail];
//	buffer->tail = (buffer->tail +1)%buffer->length;
//	return tempdata; //read the previous data, because we cant increment after a "return" statement
//	
//}
