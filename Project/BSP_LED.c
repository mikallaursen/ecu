/*
* LEDs for debugging purposes.
* The LEDS are connected to:
*
* LED 1 : PD0
* LED 2 : PD1
* LED 3 : PD2
* LED 4 : PD3
*/

/* Includes --------------------------------------------------------*/
#include "BSP_LED.h"

/* Configuration ---------------------------------------------------*/
void BSP_LED_init()
{
	GPIO_InitTypeDef GPIO_initStruct;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin = LED1 | LED2 | LED3 | LED4 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_5 | GPIO_Pin_7 | GPIO_Pin_6 | GPIO_Pin_13;
	
	// Pin 7 : Rx
	// Pin 6 : Tx
	
	GPIO_Init(GPIOB, &GPIO_initStruct);
	
	
	
	
	
	
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_4 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_1 | GPIO_Pin_9;
	
	GPIO_Init(GPIOC, &GPIO_initStruct);

	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin = LED1 | LED2 | LED3 | LED4 | GPIO_Pin_14 | GPIO_Pin_15;
	
	GPIO_Init(GPIOD, &GPIO_initStruct);
	
	
	
	
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	
	GPIO_Init(GPIOA, &GPIO_initStruct);
	
	
	
	
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_initStruct.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_11; //enable pins for CAN1 & CAN2 tranceiver 
	
	GPIO_Init(GPIOA, &GPIO_initStruct);
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
}

void TIM4_IRQHandler(){
			__disable_irq();
	if(TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET){
		
		

		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	}
	__enable_irq();
}	


