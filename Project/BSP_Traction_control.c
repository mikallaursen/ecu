#include "appl.h"
#include "PID_Controller.h"
#include "BSP_Traction_control.h"

static float integral_max = 0; //cant have the PID give additional torque
static float integral_min = -32767; //PID couldt potentially reduce torque to zero
static float pgain = 1;
static float igain = 0;
static float dgain = 0; 

static float prev_error = 0; //initialize 
static float integral_error = 0; //initialize

float slip_gain = 0;
float rpm_wheel_error = 0;

float slip_percentage = 0; //chosen by driver and stuff

float traction_control(float Back_wheels, float Front_wheels)
{
	PID traction;
	
	
	
	slip_gain = (slip_percentage - 1);
	
	//float w1 = ((Back_L_Wheel + Back_R_Wheel)/2); //average rpm of back wheels
	//float w2 = ((Front_L_Wheel + Front_R_Wheel)/2); //average rpm of front wheels
	
	rpm_wheel_error =  ((Back_wheels) * (slip_gain)) + (Front_wheels); // e(t), the error value that goes into the PID regulator
	
	traction.integrator_max = integral_max;
	traction.integrator_min = integral_min;
	traction.integral_error = integral_error;
	traction.prev_error = prev_error;
	traction.pGain = pgain;
	traction.iGain = igain;
	traction.dGain = dgain;
	
	return updatePID(&traction, rpm_wheel_error); // PID output 
	
	
}

