#include "BSP_Interrupt.h"
#include "stm32f4xx.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "misc.h"
#include "BSP_LED.h"
#include "BSP_CAN.h"
#include "BSP_Startup.h"
#include "CAN_ID_LIST.h"
#include "appl.h"

#define PING_AND_FLAGS  ping_sm_pedal && ping_sm_blackbox && ping_sm_battery && ping_rpi_comm
#define PING_OR_FLAGS  ping_sm_pedal || ping_sm_blackbox || ping_sm_battery || ping_rpi_comm

uint8_t data[1] = {1};  //an array of length 1 with the value 1 (logic high)
												//because the CAN_TX function takes in an array of max length, 1 byte
uint8_t PDM_comparator = 0; 

uint8_t ping							  = 0;
uint8_t ping_sm_pedal    		= 0;
uint8_t ping_sm_blackbox 		= 0;
uint8_t ping_sm_battery  		= 0;
uint8_t ping_rpi_comm    		= 0;
uint8_t pings_ok 	       		= 0;
uint8_t fault_ping 					= 0;
uint8_t ping_sent_no_return = 0;


uint16_t prescaler_value = 0;
uint8_t period = 0;

/*
* PC8  : Cooling ON/OFF
* PC9  : Cooling regulation ON/OFF
* PC12 : START button
* PC13 : STOP button
*/

uint8_t data_request_sent = 0;
uint8_t disable_batt_sens = 0; //to not spam the can bus with messages, only send one.

void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
void BSP_Interrupt_init(void);

void BSP_Interrupt_init(void){
	GPIO_InitTypeDef GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	EXTI_InitTypeDef EXTI_InitStruct;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource4); //Tell system PC4 is EXTI_Line4;
	
	EXTI_InitStruct.EXTI_Line = EXTI_Line4; 
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI4_IRQn; //handling interrupt from PC4 (sensor from power distribution module)
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
	
}


void EXTI4_IRQHandler(void){
	__disable_irq();
	if(EXTI_GetITStatus(EXTI_Line4) != RESET){
		
		
		
		if(disable_batt_sens == 0 && PDM_comparator == 0){
			PDM_comparator = 1; // if signal from comparator is received, something is going on with the 12V battery (operation not optimized)
			
			//CANTX(ERROR MSG);
			
			disable_batt_sens = 1; 
		} else if(disable_batt_sens == 1 && PDM_comparator == 0) {  //if 'disable_batt_sens' has gone high, it means there was an error for a bit, but then the comparator adjusted and it works as normal now
			disable_batt_sens = 0; 
		}

		EXTI_ClearITPendingBit(EXTI_Line4);
	}
	__enable_irq();
}




void BSP_timer_init(void){
	TIM_TimeBaseInitTypeDef TIM_InitStruct;
	NVIC_InitTypeDef NVIC_initStruct;



	// TIMER 3 (16 bit, have to prescale for low frequencies)
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_InitStruct.TIM_Prescaler = 6; 													
	TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_InitStruct.TIM_Period =  59999; 
	TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_InitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM3, &TIM_InitStruct);
	TIM_Cmd(TIM3,ENABLE);

	

	NVIC_initStruct.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_initStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_initStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_initStruct.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_initStruct);

	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
		  //period = SysClk / (freq - 1)
		// i want a frequency of 200Hz -> 200 times per second 
		// 84MHz / (200 -1) = 422110. this value is greatly lower than 2^32 -1. so this should work
		//if I were to use a 16 bit timer I would have to choose a higher prescaler and 
		// and therefore get a lower period (16 bits = 65535, so our period must be lower than this)
		// want the prescaler as low as possible because of propagation delay. 
		// 1 Hz -> period 55962 (1500 prescaler)
	
	
	
		// TIMER 2 (32 bit)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
		
		

	
		TIM_InitStruct.TIM_Prescaler = 1500; 
		TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up; 
		TIM_InitStruct.TIM_Period =  55962; 
    TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
		TIM_InitStruct.TIM_RepetitionCounter = 0;
		TIM_TimeBaseInit(TIM2, &TIM_InitStruct);
		TIM_Cmd(TIM2,ENABLE);
		
		


	NVIC_initStruct.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_initStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_initStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_initStruct.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_initStruct);

	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
		
	}

	
void TIM3_IRQHandler(){
			__disable_irq();
	if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET){
		
		GPIO_ToggleBits(GPIOA,GPIO_Pin_7);
		BSP_CAN_Tx(ECU_DATA_REQUEST, 1,0);

		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	}
	__enable_irq();
}	
	
	
//the continious ping function	
void TIM2_IRQHandler(){
	__disable_irq();
	if(motor_ARMED == 0) //only do this in startup mode
	{
			if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET){

				BSP_CAN_Tx(PING_SENT, 1,0);
				
		
				if(PING_OR_FLAGS){ //check if any ping flags is returned
							ping_sent_no_return = 0; //ping sent and ping received, no return variable is set to zero
							if(PING_AND_FLAGS){ //checks if all flags are set 
								
								BSP_CAN_Tx(PINGS_OK,1,0);
								pings_ok = 1;
							} else { //some pings are received, but not all
								if(fault_ping > 5) BSP_CAN_Tx(FIVE_PINGS_FAULT,0,0); 
								if(fault_ping > 10) BSP_CAN_Tx(TEN_PINGS_FAULT,0,0);
								if(ping_sm_pedal == 0) fault_ping++;
								if(ping_sm_blackbox == 0) fault_ping++;
								if(ping_sm_battery == 0) fault_ping++;
								if(ping_rpi_comm == 0) fault_ping++;
							}
					} else {
						if(ping_sent_no_return >= 10){
							BSP_CAN_Tx(NO_PING_RETURNED, 1,0);
							ping_sent_no_return = 0;
						}
						ping_sent_no_return++;
					}			
				
		
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		}
	}
		

		__enable_irq();
}

