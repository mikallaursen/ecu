#include "BSP_CAN.h"
#include "stdint.h"
#include "stm32f4xx_can.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "BSP_USART.h"
#include "BSP_systick.h"
#include "BSP_Interrupt.h"
#include "CAN_ID_LIST.h"
#include "BSP_Traction_control.h"
#include "appl.h"

uint8_t start_button = 0;
uint8_t stop_button = 0;

uint8_t calib_request_id = 0;

uint8_t can_id = 0;

uint8_t new_throttle_data = 0;


/* 
* RPI Comm board & ECU board & STM32F4-Discovery board
* PB8 : CAN1 RX  
* PB9 : CAN1 TX
*
* ECU board
* PB5 : CAN2 RX
* PB6 : CAN2 TX
*/



uint8_t data_temp[200];
uint8_t data_brake[200];
uint8_t data_throttle[200];
uint8_t data_front_wheels[200];
uint8_t data_back_wheels[200];
uint16_t data_current_sens = 0;
uint16_t data_voltage_sens = 0;

uint8_t brake_light_cutoff = 50;
uint16_t brake_value;

uint8_t new_temp_data = 0;
uint8_t new_brake_data = 0;

uint8_t i = 0;
uint8_t k = 0;
uint8_t m = 0;



int brake_breakpoint_value = 100; 
uint8_t RTDS_ACTIVATE = 0;



GPIO_InitTypeDef  GPIO_InitStructure;
CAN_InitTypeDef CAN_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;
CAN_FilterInitTypeDef CAN_FilterInitStructure;

void BSP_CAN_init(void){ // CAN Setup
	/* Have to set the can tranceiver EN pin high to enable it */
	GPIO_SetBits(GPIOA, GPIO_Pin_12); //CAN1 Enable on ECU board

	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;  //CAN RX: PB8,     CAN TX: PB9
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 8 & 9. 
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1);
	
	//CAN1 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

	/* CAN1 register init */
	CAN_DeInit(CAN1);

	/* CAN1 cell init */
	
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;                    //1tq;

	/* CAN1 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/        //30MHz 1MBps
	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;                    //6tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                    //8tq;
	CAN_InitStructure.CAN_Prescaler =(42000000 / 7) / 500000;                    //
	CAN_Init(CAN1, &CAN_InitStructure);

	/* CAN1 reset */

	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
	

	
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
}



uint8_t BSP_CAN_Tx(uint32_t address, uint8_t length, uint8_t data[8]) {
	CanTxMsg txmsg;
	txmsg.StdId = address;
	txmsg.IDE = CAN_Id_Standard;
	txmsg.RTR = CAN_RTR_Data;
	txmsg.DLC = length;
		
	for(i=0; i<length;i++){
		txmsg.Data[i] = data[i];
	}
	return CAN_Transmit(CAN1, &txmsg);
}





void BSP_CAN2_init(void){ // CAN Setup
	/* Have to set the can tranceiver EN pin high to enable it */
	GPIO_SetBits(GPIOA, GPIO_Pin_11); //CAN2 Enable on ECU board
	
	
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;  //CAN2 RX: PB5,     CAN2 TX: PB6
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 5 & 6. 
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_CAN2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_CAN2);
	
	//CAN2 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);

	/* CAN2 reset */
	CAN_DeInit(CAN2);

	/* CAN2 cell init */
	
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;                    //1tq;

	/* CAN2 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/        //30MHz 1MBps
	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;                    //6tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                    //8tq;
	CAN_InitStructure.CAN_Prescaler =(42000000 / 7) / 500000;                    //
	CAN_Init(CAN2, &CAN_InitStructure);

	/* CAN2 filter init */

	CAN_FilterInitStructure.CAN_FilterNumber = 14; //filter number for CAN1 must be 0..13    and filter number for CAN2 must be 14..27
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN2, CAN_IT_FMP0, ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = CAN2_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
}

uint8_t BSP_CAN2_Tx(uint32_t address, uint8_t length, uint8_t data[8]) {
	CanTxMsg txmsg;
	txmsg.StdId = address;
	txmsg.IDE = CAN_Id_Standard;
	txmsg.RTR = CAN_RTR_Data;
	txmsg.DLC = length;
	
	for(k=0; k<length;k++){
		txmsg.Data[k] = data[k];
	}
	return CAN_Transmit(CAN2, &txmsg);
}





void CAN1_RX0_IRQHandler(void)
{		
	CanRxMsg rxmsg;
	
	__disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0) 
	{ 					 //checks if a message is pending
		CAN_Receive(CAN1, CAN_FIFO0, &rxmsg);    //receiveing message on can1, First In First Out filter
		
	
		if(rxmsg.StdId == START_BUTTON_PRESSED) 
		{
			start_button = 1;
			stop_button = 0;
			RTDS_ACTIVATE = 1;
			
		}
		
		if(rxmsg.StdId == STOP_BUTTON_PRESSED)
		{
			start_button = 0;
			stop_button = 1;
		}
		
		if(rxmsg.StdId == FRONT_WHEELS)
		{
			data_front_wheels[0] = rxmsg.Data[0];
			
		}
		
		if(rxmsg.StdId == BACK_WHEELS)
		{
			data_back_wheels[0] = rxmsg.Data[0]; //assuming the average value given is in RPM. if not, then change code accordingly
		}
		
		if(rxmsg.StdId == CURRENT_SENSOR)
		{
			data_current_sens = rxmsg.Data[0] | rxmsg.Data[1];
		}
		
		if(rxmsg.StdId == VOLTAGE_LEVEL)
		{
			data_voltage_sens = rxmsg.Data[5] | rxmsg.Data[6];
			data_voltage_sens = (data_voltage_sens / 2 ); //from 2^16 to 2^15 bits, which corresponds to 24-1250 Volts down to 24-613 Volts.
			
		}
		
		if(motor_ARMED == 0)
		{  //startup or waiting for user commands
			if(rxmsg.StdId == SLIP_PERCENTAGE)
			{
				slip_percentage = rxmsg.Data[0];
			}
			/* Check if calibration is requested*/
			if(rxmsg.StdId == CAL_REQUEST)
			{
					rxmsg.Data[0] = calib_request_id; //either 0, 1, 3, 4 etc. where 0 is no calibration, and the other integers are the chosen sensors
			}
			
			if(rxmsg.StdId == PRECHARGE_DONE)
			{
				precharge_done = 1;
			}
			
		}
		
		if(motor_ARMED == 1)
		{
		
			
			
//			/* Torque to motor */
			if(rxmsg.StdId == THROTTLE_POSITION_SENSOR) {  //n�r mottat ID stemmer overens med v�r definerte ID
				
				data_throttle[0] = rxmsg.Data[0];
				data_throttle[1] = rxmsg.Data[1];
				
		//		temp_rpm =  (int16_t) ((rxmsg.Data[1]<<8) | rxmsg.Data[0]); 
				new_throttle_data = 1;
			}
			
			if(rxmsg.StdId == FRONT_WHEELS || rxmsg.StdId == BACK_WHEELS)
			{
					
			}
			

		}

		if(rxmsg.StdId == BRAKE_PRESSURE_FRONT || rxmsg.StdId == BRAKE_PRESSURE_BACK) 
		{
			brake_value = rxmsg.Data[1] << 8 | rxmsg.Data[0]; // CAN-buss meldingene er 8 bit, antar at bremsesensor har 12bit eller mer.
			
			if(brake_value > brake_light_cutoff){
				GPIO_SetBits(GPIOC, GPIO_Pin_3);
				}
			GPIO_ResetBits(GPIOC, GPIO_Pin_3);
		}
		
	}
	__enable_irq();
}

	


void CAN2_RX0_IRQHandler(void)
{
if(CAN2->RF0R & CAN_RF0R_FMP0) { 					 //checks if a message is pending
CanRxMsg rxmsg;
		CAN_Receive(CAN2, CAN_FIFO0, &rxmsg);    //receiveing message on can1, First In First Out filter
		GPIO_SetBits(GPIOA, GPIO_Pin_6);
		
//	if(*rxmsg.Data == 0x0B) GPIO_SetBits(GPIOA, GPIO_Pin_5);  //PA5 : LED 3 p� ECU
}
}		
	
	
	



