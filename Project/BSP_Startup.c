#include "BSP_Startup.h"
#include "BSP_systick.h"
#include "BSP_CAN.h"
#include "BSP_USART.h"

CanRxMsg can1rx;
//maybe check_rrors should also check temperature from battery and state of charge and such?
// if everything is OK then set an flag = 1 
// and after the user has chosen to calibrate or not calibrate sensors, another flag is set
// 

uint8_t fault_check = 0;
uint8_t calibrating_done = 0;
uint8_t precharge_done = 0;
uint8_t startup_done = 0;


void BSP_Startup_startup_routine(void){
	
	if(precharge_done && calibrating_done){
		startup_done = 1;
	}

		/* calibrating_done = 1    when calibrating is done
			 precharge_done   = 1    when precharge_done signal received
			
		   So, if not all of these are 1, u are stuck in the loop untill everything is OK
		*/
		
		// if user has pressed the STOP BUTTON, this loop will run again. if the user decides to calibrate, then he/she can do so.
			//if calibrating is done, user will not enter this loop again. just waiting for safety checks
						BSP_Startup_calibrate();
			
			//check once again if everything is alright.
			
}


//TESTING VARIABLES
uint8_t cal_throttle_data = 0;

		uint8_t throttle_MAX = 0;
		uint8_t throttle_MIN = 0;
		uint8_t break_MAX = 0;
		uint8_t break_MIN = 0;

void BSP_Startup_calibrate(void){
	while(calibrating_done == 0){ //stay in this while loop untillthe user presses a button on the display to signal that calibrating is done
		
		//this switch will handle all the different sensors to calibrate
		switch(calib_request_id){ //where can id is the users input. for example if the user wants to calibrate throttle position sensor. then that switch case will match usart_id
			case'1': //throttle sensor ID
				while(throttle_MAX == 0 && throttle_MIN == 0){ //stuck in this while loop untill user says calibration is OK for brake MAX and brake MIN
																			 // "calibration successfull" will be received over CAN bus from the sensor modules
					if(cal_throttle_data == 0) {
				//			calibrate max position for the sensor.
						} else if(cal_throttle_data == 2) {
				//		calibrate minimum position for the sensor. 
					}
					
					if(cal_throttle_data == 1) throttle_MAX = 1;
					if(cal_throttle_data == 3) throttle_MIN = 1;
					
				}
				break;
				
			case'2': //break sensor ID 
				while(break_MAX == 0 && break_MIN == 0){
				//process received data from CAN1 bus
				}
			break;
				
			case'3':
				break;
			
			case'0':
				calibrating_done = 1; // when user is satisfied with calibrating, this will break 
				break;
			
		}
	}
}