#include "stdint.h"
#include "stm32f4xx_can.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

extern uint8_t precharge_done;
extern uint8_t BSPD_fault;
extern uint8_t IMD_fault;
extern uint8_t BMS_fault;
extern uint8_t TPOS_fault;

extern uint8_t cal_throttle_data;
extern uint8_t cal_break_data;


extern uint8_t data_throttle[200];
extern uint8_t data_temp[200];
extern uint8_t data_brake[200];
extern uint8_t data_front_wheels[200];
extern uint8_t data_back_wheels[200];
extern uint16_t data_current_sens;
extern uint16_t data_voltage_sens;


extern uint8_t new_temp_data;
extern uint8_t new_brake_data;

extern uint8_t start_button;
extern uint8_t stop_button;

extern uint8_t can_id;

extern uint8_t calib_request_id;

extern uint8_t RTDS_ACTIVATE;

extern uint8_t new_throttle_data;


/* temporary IDs to test code */
#define CAN1_RX_BMS_FAULT 0x00
#define CAN1_RX_IMD_FAULT 0x01
#define CAN1_RX_BSPD_FAULT 0x02
#define CAN1_RX_TPOS_FAULT 0x03
#define CAN1_RX_PRECHARGE_DONE 0x04
#define CAN1_RX_TEMP 0x05
#define CAL_THROTTLE 0x06
#define CAL_BREAK 0x07

uint8_t BSP_CAN_Tx(uint32_t address, uint8_t length, uint8_t data[8]);
uint8_t BSP_CAN2_Tx(uint32_t address, uint8_t length, uint8_t data[8]);
void BSP_CAN_init(void);
void BSP_CAN2_init(void);
